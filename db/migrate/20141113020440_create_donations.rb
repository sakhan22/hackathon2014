class CreateDonations < ActiveRecord::Migration
  def change
    create_table :donations do |t|
      t.decimal :amount
      t.date :donation_date
      t.integer :donor_id

      t.timestamps
    end
  end
end
